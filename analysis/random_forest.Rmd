---
title: "Random forest"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: true
---

# Setup

```{r, include=F}
library(purrr)
devtools::load_all()
library(tictoc)
tic("Overall running time")
```

Define all the outcome variables that should be analyzed

```{r}
outcome_vars <- c("qc_co2_flux", "flag_timelag_sf_co2", "flag_absolute_limits_hf_co2", "flag_spikes_hf_co2")
```


```{r, echo=FALSE, results='asis'}
src <- map(outcome_vars, function(outcome) {
  knitr::knit_expand(
    here::here('analysis/child/random_forest_categ_template.Rmd'), outcome = outcome
  )
})
res <- knitr::knit_child(text = unlist(src), quiet = TRUE)
cat(res, sep = '\n')
```

# Timing

```{r}
toc()
```