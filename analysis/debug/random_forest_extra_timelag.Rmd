---
title: "Random forest"
output: html_notebook
---

```{r, include=F}
source(here::here("setup_libs.R"))
library(rpart.plot)
library(tictoc)
library(doFuture)
library(knitr)
library(ranger)
```

```{r}
ec_raw <- here("data/1_prepared/ec_with_na_hainich.rds") %>%
        read_rds() 
```

# Random Forest flag_timelag_sf_co2


Remove NAs from the dataset

```{r}
ec <- ec_raw %>% 
  drop_na()
```

## Dataset split

```{r}
ec_split_flag_timelag_sf_co2 <- initial_split(ec, prop = .7)

ec_train_flag_timelag_sf_co2 <- training(ec_split_flag_timelag_sf_co2)
ec_test_flag_timelag_sf_co2 <- testing(ec_split_flag_timelag_sf_co2)
```


## Basic recipes

```{r}
flag_timelag_sf_co2_rf_rec <- recipe(ec_train_flag_timelag_sf_co2) %>% 
  update_role(starts_with("flag"), starts_with("qc"), new_role = "flag") %>% # mark them as flag so is not incluced in computation
  update_role(!has_role("flag"), new_role = "predictor") %>% 
  update_role(time, new_role = "time") %>% 
  add_role(flag_timelag_sf_co2, new_role="outcome")
```

```{r}
tree_model <- rand_forest(mode="classification") %>% 
  set_engine("ranger", importance = "impurity", num.threads = NULL)
```

```{r}
flag_timelag_sf_co2_rf_wflow <- workflow() %>% 
  add_recipe(flag_timelag_sf_co2_rf_rec) %>% 
  add_model(tree_model)
```

```{r, collapsible=TRUE}
summary(flag_timelag_sf_co2_rf_rec) %>% 
  filter(role == "predictor") %>% 
  kable()
```

## Results flag_timelag_sf_co2

```{r}
tic("Basic random forest fit")
flag_timelag_sf_co2_rf_fit <- flag_timelag_sf_co2_rf_wflow %>% 
  fit(ec_train_flag_timelag_sf_co2)
toc()
```


### Variable importance


```{r}
flag_timelag_sf_co2_rf_fit %>% 
  extract_fit_engine() %>% 
  importance()
```

Variable importance (as a fraction of the total) for the random forest model

```{r, collapsible=TRUE}
flag_timelag_sf_co2_rf_fit %>% 
  extract_fit_engine() %>% 
  importance() %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "variable") %>% 
  rename(importance = ".") %>% 
  mutate(importance = importance / sum(importance)) %>% 
  arrange(desc(importance)) %>% 
  mutate(variable = fct_inorder(variable)) %>% 
  kable()
```


```{r, fig.cap="Top 30 variables for importance for flag_timelag_sf_co2"}
flag_timelag_sf_co2_rf_fit %>% 
  extract_fit_engine() %>% 
  importance() %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "variable") %>% 
  rename(importance = ".") %>% 
  mutate(importance = importance / sum(importance)) %>% 
  arrange(importance) %>% 
  slice_tail(n = 35) %>% 
  mutate(variable = fct_inorder(variable)) %>% 
  ggplot(aes(importance, variable)) +
  ggalt::geom_lollipop(horizontal = T)
```


### Summary

whole summary of the partition tree

```{r}
flag_timelag_sf_co2_rf_fit %>% 
  extract_fit_engine()
```



## Model accuracy flag_timelag_sf_co2

```{r}
flag_timelag_sf_co2_rf_pred <- flag_timelag_sf_co2_rf_fit %>% 
  augment(ec_test_flag_timelag_sf_co2)
```

## Metrics

```{r}
flag_timelag_sf_co2_rf_pred %>% 
  metrics(flag_timelag_sf_co2, .pred_class)
```


### Confusion matrix


```{r}
conf_mat <- flag_timelag_sf_co2_rf_pred %>% 
  conf_mat(flag_timelag_sf_co2, .pred_class)
```

```{r}
conf_mat %>% 
  autoplot()
```

```{r}
conf_mat %>% 
  autoplot(type="heatmap")
```


### Null Model

Train a null model for comparing the accuracy

```{r}
flag_timelag_sf_co2_null_rf <- workflow() %>% 
  add_recipe(flag_timelag_sf_co2_rf_rec ) %>% 
  add_model(null_model() %>%  set_engine("parsnip")) %>% 
  fit(ec)
```

```{r}
flag_timelag_sf_co2_null_pred_rf <- flag_timelag_sf_co2_null_rf %>% 
  augment(ec)
```

```{r}
class_metrics <- metric_set(accuracy)

flag_timelag_sf_co2_null_pred_rf %>% class_metrics(truth = flag_timelag_sf_co2, estimate = .pred_class) %>% 
  kable()
```


# Tuning

```{r}
library(tidymodels)
```


```{r}
tidymodels_prefer()
```


```{r}
tree_model_tune <- rand_forest(mode="classification", mtry=tune(), min_n=tune()) %>% 
  set_engine("ranger", importance = "impurity", num.threads = NULL)
```

```{r}
(wflow_tune <- flag_timelag_sf_co2_rf_wflow %>% 
  update_model(tree_model_tune))
```

```{r}
ec_fold <- vfold_cv(ec, v=4, repeats = 2)
```

```{r}
param <- wflow_tune %>% 
  parameters() %>% 
  finalize(ec)
```

```{r}
grid <- grid_regular(param)
```


```{r}
wflow_tuned <- wflow_tune %>% 
  tune_grid(
    resamples = ec_fold,
    grid = grid)
```

```{r}
wflow_tuned
```
```{r}
wflow_tuned %>% 
  autoplot()
```
```{r}
wflow_tuned %>% 
  show_best()
```


# ```{r}
# ctrl <- control_bayes(verbose = TRUE)
# 
# set.seed(1403) 
# wflow_tuned <- wflow_tune %>% 
#   tune_bayes(
#     resamples = ec_fold,
#     control = ctrl
#   )
# ```

```{r}
work
```


