---
title: "Assessing Opportunities of Machine Learning for Quality Management in Eddy Covariance Measurements"
subtitle: "Ecosystem Analysis and Modelling Project 2022 - University Göttingen"
author: "Simone Massaro <br/> Supervisor: Dr. Franziska Koebsch"
date: "1st February 2022"
bibliography: "../EC_data_quality_bibliography.bib"
output:
  ioslides_presentation:
    widescreen: true
    smaller: true
    css: style.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = F, cache = T)
library(tidyverse)
```

# Introduction

## Eddy Covariance (EC)

Eddy Covariance (EC) is a state of the art technique to measure turbulent exchanges between the Ecosystem and the Atmosphere. Works by measuring **covariance** between **vertical wind** speed and **gas** concentration.

- single point measurement of ecosystem scale . 
- several gas fluxes can be measured (eg. $CO_2$, $H_2O$).
- continuous and automated measurement.

<div class="centered">
![Source @burba_Eddy_2013.](`r here::here("presentation/figures/EC_example_burba.png")`){width=65%}
</div>

## EC Assumptions

<div style="display: flex; justify-content: space-between">

<div>

EC Processing:

- instruments measurements at 20 Hz.
- aggregated into fluxes for every 30 mins.
- several **assumptions** need to be fulfilled for correct calculations

Conditions hamper assumptions:

- predominance advective over **turbulent** transport. 
- disturbance of turbulent field.

Conditions hamper instruments:

- rain (eg. sonic anemometers).
- frost.
- high humidity.

Several **Quality Control** flags to monitor EC data quality 

</div>

<div style="max-width: 40%">
![Example EC setup in Hainich. Source: bioclimatology department Uni Göttingen.](`r here::here("presentation/figures/anemometer.JPG")`){width=75%}
</div>
</div>

## Goal of the project

Aims:

 - understand relationship between meteorological conditions and EC data quality. 
 - accurately define critical meteorological conditions for measurements.
 - understand the potential and limits of machine learning for EC data quality management.

Relevance:

- integrated approach (current  Quality Control schemes focus only on individual flags).
- understand mechanism that connect meteo conditions and EC data quality.
- results can be used to optimize field measurement design and post-processing.
- data-driven and systematic assessment of EC data quality.



# Methods

## Data

- data from EC tower in Hainich national park.
- period: 2016 - 2020. 
- meteo and EC data.

<div class="centered" style="margin: 20pt">
![Main EC setup in Hainich. Source: bioclimatology department Uni Göttingen](`r here::here("presentation/figures/hainich.JPG")`){width=60%}
</div>

## QC $CO_2$ Flux

Quality control flag for atmospheric conditions [@foken_PostField_2005].

**Combined** flag of two statistical tests:

 - Steady state tests.
 - Test on developed turbulent conditions.
 
Result flag is:

- 0 - best quality data.
- 1 - data should be used carefully.
- 2 - data should be discharged.

Hypothesis on flag occurrence:

  - low turbulence (e.g. advective conditions at night).
  - rapid change in atmospheric conditions.
  


## Timelag $CO_2$

Timelag between the timeseries of anemometer and the gas analyzer.

Reason:

 - atmosphere decoupling.

Hypothesis on flag occurrence:

- wind direction: tower structure disturbs turbulent field (South East for Hainich).

<br/>
<br/>

<div class="centered">
![Timelag between two timeseries. Source: @eddypro7_stat_test](`r here::here("presentation/figures/timelag.png")`)
</div>

## Absolute limits $CO_2$

Absolute limit checks if measurements are inside plausible range.

Reason:

- instrument malfunction.

Hypothesis on flag occurrence:

- rain.
- high humidity.

<div class="centered">
![Source: @eddypro7_stat_test](`r here::here("presentation/figures/abs_limits.png")`)
</div>

## Data properties

**Predictors used in the model**

|                |        |                  |              |             |
|----------------|--------|------------------|--------------|-------------|
| wind_speed     | RH     | co2_mixing_ratio | TA_diff      | LE          |
| max_wind_speed | SW_IN  | air_temperature  | miss_records | bowen_ratio |
| w_unrot        | SW_OUT | co2_strg         | P            | v_var       |
| H              | LW_IN  | z_d_L            | u_star       | NETRAD      |
| w_var          | LW_OUT | e                | TKE          | u_var       |

<br/>
**Data properties** after data cleaning and preprocessing.

```{r, echo=F, cache=TRUE}
ec <- here::here("data/1_prepared/ec_with_na_hainich.rds") %>%
        read_rds()
tribble(
  ~description, ~value,
  "Number of observations", as.character(nrow(ec)),
  "Number of variables", as.character(ncol(ec)),
  "Frequency of data", "30 mins"
) %>% 
  knitr::kable(col.names = c("", "")) 
```

## Model Approach

Two classification approaches

|   LDA (Linear Discriminant Analysis)  |  Decision Tree    |
|-----------------------|--------------------|
| | |
|  - good interpretation (coefficient for each predictor).<br/> - basic patterns and redundancies in data.<br/> - limited to linear relationships. <br/> - requires data preprocessing.| - good interpretation (explicit binary split based on thresholds). <br /> - handles complex relationships. </br > - no data preprocessing. |                     
| ![LDA QC $CO_2$](`r here::here("public/figure/LDA_all.Rmd/qc_co2_flux-lda-complete-1.png")`){width=80%}       | ![Decision tree QC $CO_2$](`r here::here("public/figure/decision_trees.Rmd/qc_co2_flux-main-tree-1.png")`){width=80%} |

# Results

## Timelag LDA

<div class="two-cols">
<div>
![LDA 1 component distribution timelag $CO_2$](`r here::here("public/figure/LDA_all.Rmd/flag_timelag_sf_co2-lda-complete-1.png")`){width=100%}
</div>
<div class="sidebar">
<img width=350 src="../public/figure/LDA_all.Rmd/flag_timelag_sf_co2-conf-mat-1.png">
<img width=200 src="figures/timelag.png">
</div>

</div>
## Timelag LDA scaling

<div class="two-cols">
<div>
![Scaling LDA timelag $CO_2$](`r here::here("public/figure/LDA_all.Rmd/flag_timelag_sf_co2-lda-scaling-1.png")`){width=90%}
</div>
<div class="sidebar">
<img width=200 src="figures/timelag.png">
<div>

The occurrence of `timelag flag` is higher when:

- TKE (Turbulent Kinetic Energy) is high.
- variance of horizontal wind (`u_var`, `v_var`) is low.
- no influence of wind direction.
</div>
</div>


## Timelag decision tree
<div class="two-cols">
<div>
![Decision tree timelag $CO_2$](`r here::here("public/figure/decision_trees.Rmd/flag_timelag_sf_co2-main-tree-1.png")`){width=100%}
</div>
<div class="sidebar">
<img width=350 src="../public/figure/decision_trees.Rmd/flag_timelag_sf_co2-conf-mat-1.png">
<img width=200 src="figures/timelag.png">
</div>


## Timelag decision tree
<div class="two-cols">
<div>
![Decision tree timelag $CO_2$](`r here::here("public/figure/decision_trees.Rmd/flag_timelag_sf_co2-main-tree-1.png")`){width=90%}
</div>
<div class="sidebar">
<img width=200 src="figures/timelag.png">
<div>
The occurrence of `timelag` is higher when:

- Incoming shortwave radiation is low (night).
- there are more than 2 missing records.
</div>
</div>

## Timelag decision tree: variable importance

<div class="two-cols">
<div>
![Decision tree: variable importance for timelag $CO_2$ with cross validation ](`r here::here("public/figure/decision_trees.Rmd/importance-cross-flag_timelag_sf_co2-1.png")`){width=90%}
</div>
<div class="sidebar">
<img width=200 src="figures/timelag.png">
<div>
- radiation variables are all important (night indicator).
- missing records.
- turbulence parameters (`L`, `z_d_L`) also important.
</div>
</div>


## QC LDA

<div class="two-cols">
<div>
![Scaling LDA QC $CO_2$](`r here::here("public/figure/LDA_all.Rmd/qc_co2_flux-lda-scaling-1.png")`){width=95%}
</div>
<div class="sidebar">
<img width=300 src="../public/figure/LDA_all.Rmd/qc_co2_flux-conf-mat-1.png">
</div>



## QC decision tree

<div class="two-cols">
<div>
![Decision tree QC $CO_2$](`r here::here("public/figure/decision_trees.Rmd/qc_co2_flux-main-tree-1.png")`){width=95%}
</div>
<div class="sidebar">
<img width=300 src="../public/figure/decision_trees.Rmd/qc_co2_flux-conf-mat-1.png">
</div>
# Conclusion

## What we learned so far

Study design:

- it is possible to predict QC flags from meteo data (accuracy still limited).
- need to better understand use of the two models (LDA, Decision Tree).

Models agreement:

- disagreement on variable importance.
- models have very different mechanics.

Why some flags work better:

- meteo variable may not be enough for predicting flag (eg. instrument issues for absolute limits).
- models used may be not complex enough to capture the interaction between variables.


## Next steps


- random forest model (ensemble of decision trees)
    - model with increased prediction power.
    - more difficult to interpret.
- apply same approach to other EC quality flags.
- consult with domain expert to improve model and how it can be used.

## Code Availability

- all notebooks of analysis: [https://mone27.gitlab.io/ec-data-quality](https://mone27.gitlab.io/ec-data-quality)
- code developed for analysis: [https://mone27.gitlab.io/ec-data-quality/package](https://mone27.gitlab.io/ec-data-quality/package)
- this presentation: [https://mone27.gitlab.io/ec-data-quality/presentation_project_1st_feb.html](https://mone27.gitlab.io/ec-data-quality/presentation_project_1st_feb.html)


source code: [https://gitlab.com/mone27/ec-data-quality](https://gitlab.com/mone27/ec-data-quality)


## References


