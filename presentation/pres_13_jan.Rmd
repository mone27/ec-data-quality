---
title: "Analysis EC quality flags"
author: "Simone Massaro"
date: "13 January 2022"
output:
  ioslides_presentation: default
  revealjs::revealjs_presentation: default
  slidy_presentation: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = F, cache = T)
```

```{r lib-setup, include=FALSE, cache=FALSE}
library(tidyverse) 
library(patchwork)
theme_set(theme_bw())
library(kableExtra)
library(tidymodels)
library(ggthemes)
devtools::load_all()
```


```{r load-notebooks, include=FALSE, cache=TRUE}
# placeholder
source_rmd(here::here("LDA/5_LDA.Rmd"))      
source_rmd(here::here("PCA/PCA_flags.Rmd"))
source_rmd(here::here("LDA/6_abs_limit.Rmd"))
```


## Introduction

The aim of the project is to gain a more systematic understanding of limitations of EC measurements under different environmental conditions and to more accurately define critical situations that lead to poor quality data.   

Identify specific environmental conditions that can hamper EC measurement quality. This can later be included into data cleaning pipelines.     

No previous literature has been found with this specific aim.

To achieve this goal a series of models are built to predict the EC data quality flags from the meteorological conditions, and then the model are interpreted to analyzed which conditions leads to poor data quality.

## Data

The data used in the project is from the tower in Hainich national park, from 2016 till 2020.
from two source:

- EC data, from a Gill-HS anemometer and a Li-7200 gas analyzer then processed with EddyPro
- Meteo data from the station



## Flag selection

The EddyPro output has `r ec %>% select(starts_with("flag"), starts_with("qc")) %>%  length` flag variables.  
Each variable (eg. co2 flux) has two set of flag:

- Turbulent flux. Quality flags of Mauder & Foken 2004
- Statistical tests on high frequency data

As a start, the analysis was focused on the co2 flux and 3 flags were selected based on their relevance in the data set

- `qc_co2_flux`
- `flag_timelag_sf_co2`
- `flag_absolute_limits_hf_co2`

   

## Flag distribution

```{r, fig.cap="Distribution of the 3 flags of interest in the whole dataset."}
p1 <- ec_f_freq %>%
  filter(flag %in% c("flag_timelag_sf_co2", "flag_absolute_limits_hf_co2")) %>% 
  plot_flags(var_name = "")
p2 <- ec_f_freq %>%
  filter(flag == "qc_co2_flux") %>% 
  plot_qc_flags(var_name = "")

p1 / p2 
```
## LDA

First step explanatory classification model: 

- LDA (Linear Discriminant Analysis). LDA it can considered as a supervised version of PCA, that combines both classification and redundancy analysis. LDA finds new features by the linear combination of the predictors in order to maximize the separation of the outcome variable.

   
For the LDA model only a subset of the meteorological variables are used in order to simplify the analysis.

## Variables included

```{r}
sel_cols <- predictor_names(lda_rec_final)
#sel_cols <- names(lda_data)
tibble(c1 = sel_cols[1:(length(sel_cols) / 2)], c2 = sel_cols[(length(sel_cols)/2 +1):length(sel_cols)] ) %>% 
  kable(col.names = c("", ""), caption = "Predictors used in the LDA models. The wind direction has been encoded into 8 sectors") %>% 
  kable_styling(font_size = 20, full_width=T) %>% 
  column_spec(1:2, width="60%") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```


## Preprocessing

Before the LDA the data has been preprocessed:

- Removing rows that contain `NA`
- filtering to remove outliers (removing top and bottom `3%` of quantiles)
- transforming with YeoJohnson to change the skweness of the distribution to better approximate normal distributions
- normalize (needed as with LDA all variables should be in the same scale)

## LDA QC CO2

```{r, fig.cap="Scatter plot for LD1 and LD2 axis and desity distribution fo different classes. LDA analysis of QC CO2"}
plot_lda_complete(qc_model)
```

## LDA QC CO2 scaling

```{r, fig.cap="Plot of scaling values (linear coefficient for LDA) for all predictores. LDA analysis of QC CO2"}
plot_lda_scalings(qc_model)
```

## LDA QC CO2 Confusion Matrix

```{r, fig.cap="Plot of confusion matrix for LDA QC Model. The percentages are calculated as a fraction of the total number of truth values"}
qc_pred %>% 
  plot_conf_mat_perc(qc_co2_flux, .pred_class) 
```

## LDA QC model accuracy

```{r}
qc_pred %>% 
  lda_metrics(truth = qc_co2_flux, estimate = .pred_class) %>% 
  select(.metric, .estimate) %>% 
  kable(col.names = c("", ""), caption = "Metrics of LDA QC Model") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```
    
    
    
```{r}
qc_null_pred %>% 
  lda_metrics(truth = qc_co2_flux, estimate = .pred_class) %>% 
  select(.metric, .estimate) %>% 
  kable(col.names = c("", ""), caption = "Metrics for Null Model for QC") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```



## Timelag LDA

```{r, fig.cap="Histogram and box plot for LD1 (first LDA axis) for the timelag presence and absence. LDA analysis of timelag CO2"}
plot_lda_complete(timelag_model)
```

## LDA Timelag scaling

```{r, fig.cap="Plot of scaling values (linear coefficient for LDA1) for all predictores. LDA analysis of timelag CO2"}
plot_lda_scalings(timelag_model)
```

## LDA Timelag model accuracy


```{r}
timelag_pred %>% 
  lda_metrics(truth = flag_timelag_sf_co2, estimate = .pred_class) %>% 
  select(.metric, .estimate) %>% 
  kable(col.names = c("", ""), caption = "Metrics of LDA Timelag Model") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```
    
    
    
```{r}
timelag_null_pred %>% 
  lda_metrics(truth = flag_timelag_sf_co2, estimate = .pred_class) %>% 
  select(.metric, .estimate) %>% 
  kable(col.names = c("", ""), caption = "Metrics for Null Model for Timelag") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```

## LDA Timelag CO2 Confusion Matrix

```{r, fig.cap="Plot of confusion matrix for LDA QC Model. The percentages are calculated as a fraction of the total number of truth values"}
timelag_pred %>% 
  plot_conf_mat_perc(flag_timelag_sf_co2, .pred_class) 
```


## Absolute limits

The performance of the LDA model for the absolute limits is very poor.

This may due to the fact that meteorological variables are not good preditors 

```{r}
abs_pred %>% 
  lda_metrics(truth = flag_absolute_limits_hf_co2, estimate = .pred_class) %>% 
  select(.metric, .estimate) %>% 
  kable(col.names = c("", ""), caption = "Metrics of LDA Absolute limits Model") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```
    
    
    

```{r}
abs_null_pred %>% 
  lda_metrics(truth = flag_absolute_limits_hf_co2, estimate = .pred_class) %>% 
  select(.metric, .estimate)  %>% 
  kable(col.names = c("", ""), caption = "Metrics for Null Model for Absolute limits") %>% 
  kable_styling(htmltable_class = "lightable-classic")
```

## Further analysis

The next steps is building a more complex model that can be better at identifying patterns into the data

possible options:

- Decision tree
- Random forest

Need to balance prediction power and interpretability.

Decision trees an provide discrete filter thresholds which are very easy to verify by expert knowledge and could be directly translated in a quality screening scheme.




