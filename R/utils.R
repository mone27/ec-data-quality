#' Print all object
#' This functions calls print in the argument after removing the limit of max number of lines can be printed
#' @param x the object to be printed
print_all <- function(x){
  prev_limit <- getOption("max.print")
  options(max.print = 9999999)
  print(x)
  options(max.print=prev_limit)
}
