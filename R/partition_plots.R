# Partition plots -----


#' Mean function that can also handle factors in a very basic way
mean_factor <- function(x){
  if (is.factor(x)){
    levels(x)[1] # return the first level: just to have a value there
  }
  else {
    mean(x)
  }
}

#' Model predictions over a grid of two variables
#' @description 
#' A grid is generatated for the `n` combinations of the `x` and `y` variable in the mapping
#' in the range of the original data
#' for the other variables the mean is taken 
#' The model is used to predict the data
#' @param wkflow a fitted workflow
#' @param data the original dataset
#' @param mapping an ggplot mapping (made with aes) with the x and y vars
#' @param n (optional) the number of points in the grid
#' @return a grid augmented with model predictions
#' @export
get_prediction_grid <- function(wkflow, data, mapping, n= 100){
  # Find the appropriate mean
  lim1 <- data %>% 
    summarize(min = min(!!mapping$x), max = max(!!mapping$x))
  lim2 <- data %>% 
    summarize(min = min(!!mapping$y), max = max(!!mapping$y))
  
  # For the other variables take the mean
  grid_base <- data %>% 
    summarize(across(c(-!!mapping$x, -!!mapping$y), mean_factor))
  # repeat to match grid dimension
  grid_base <- map(seq(1, n* n), ~grid_base) %>% 
    bind_rows()
  
  # generate the grid for the variable of interest
  grid <- expand_grid(
    !!mapping$x := seq(lim1$min, lim1$max, length = n),
    !!mapping$y := seq(lim2$min, lim2$max, length = n)) %>%
    bind_cols(grid_base)
  # augment with the workflow
  grid <- wkflow %>%
    augment(grid)
  grid
}

#' Partition plot
#' @description 
#' The partition plos is made by a scatter plot over coloured area according to the prediction of the model
#' To reduce  overplotting the number of points in the scatter is sampled
#' @param data the original dataset
#' @param mapping an ggplot mapping (made with aes) with the x and y vars
#' @param wkflow a fitted workflow
#' @param npoint (optional) the number of points in the grid
#' @return a ggplot partition plot
#' @export
plot_partition_plot <- function(data, mapping, wkflow, npoints = 100) {
  
  # Make a grid over which the background the separation is shown
  grid <- get_prediction_grid(wkflow, data, mapping)
  
  # Plot
  outcome <- outcome_names(wkflow)
  
  # Sample data to reduce number of points
  data <- data %>% 
    slice_sample(n=npoints)
  
  ggplot(data, mapping) +
    geom_tile(aes(fill = .pred_class), data = grid, alpha = .5) +
    geom_point(aes_string(col = outcome, shape = outcome)) +
    scale_fill_colorblind() +
    scale_color_colorblind()
}


#' Makes a series of partition plots for given variables
#' @description 
#' for the `vars` it makes a partition plots
#' All the variable combinations are done. For better visualization up to 4 subplots are shown in one plot
#' @param wkflow fitted workflow
#' @param data original dataset
#' @param top vector with the variables to 
plot_partition_pairs <- function(wkflow, data, vars, npoints = 200){
  
  plots <- 
    # Do a double loop to have all variable couples
    map(seq_along(vars), function(i){
      # i-1 because we don't want to take the same variable twice 
      # don't take all combinations to avoid duplicate couples
      map(seq_len(i-1), function(j){
        var1 <- vars[i]
        var2 <- vars[j]
        mapping <- aes_(as.name(var1), as.name(var2))
        plot_partition_plot(data, mapping, wkflow, npoints)
      })
    }) %>% 
    flatten() # the list is nested from the double loop
  
  # Since on screen you can't have more than 4 plots for good visuals
  # make multiple plots
  # split the array, taken from https://statisticsglobe.com/split-vector-into-chunks-in-r
  plots <- split(plots,ceiling(seq_along(plots) / 4))
  
  walk(plots, ~wrap_plots(splice(.x), guides = 'collect') %>% print())
}