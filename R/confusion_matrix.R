#' Confusion matrix with percentage
#' This is broken
conf_mat_perc <- function(data, truth, estimate, perc=T){
  perc_fact <- ifelse(perc, 100, 1) # factor to convert to percentage
  
  mat <- conf_mat(data, {{truth}}, {{estimate}})
  mat_lev <- data %>% 
    pull({{truth}}) %>% 
    levels()
  
  mat_perc <- mat %>% 
    tidy() %>% 
    mutate(value = (value / sum(value) ) %>% round(2) * perc_fact) %>% 
    pull(value) %>% 
    matrix(byrow = TRUE, ncol = length(mat_lev))
  
  colnames(mat_perc) <- mat_lev
  rownames(mat_perc) <- mat_lev
  mat_perc
}



#' Plots the confusion matrix with percentage
#' 
plot_conf_mat_perc <- function(data, truth, pred) {
  # matrix levels
  mat_lev <- data %>% 
    pull({{truth}}) %>% 
    levels()
  # The problem is that the confusion matrix has levels from 1 to n, but doesn't respect the original factors levels
  # So need to manually add manual levels
  names(mat_lev) <- 1:length(mat_lev) %>% as.character()
  
  data %>% 
    conf_mat({{truth}}, {{pred}}) %>% 
    tidy() %>% 
    mutate(pred = str_extract(name, "\\d$"), truth = str_extract(name, "\\d")) %>% 
    mutate(across(c(pred, truth), . %>% factor() %>% recode_factor(!!!mat_lev))) %>% 
    group_by(truth) %>% 
    mutate(perc = (value / sum(value) ) %>% round(3) * 100) %>% 
    ggplot(aes(truth, pred)) +
    geom_tile(aes(fill=perc)) +
    # geom_tile(aes(truth, truth), col = "green", size = .8, width=.3, height = .3, fill = NA) +
    geom_text(aes(label=perc_label(perc)), size = 15) +
    ggplot2::scale_fill_gradient(low = "grey90", high = "grey40") +
    ggplot2::theme(legend.position = "none") +
    labs(caption="Percentage are calculated with the truth values")
}

#' Proper percentage label
#' @description 
#' if perc is Nan return "-" otherwise appends "%"
perc_label <- function(perc){
  ifelse(!is.nan(perc), paste0(perc, "%"), "-")
}
