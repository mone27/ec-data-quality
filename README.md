# ec-data-quality

This is repository containing all the code for EC data quality rearch project.

For more details check the website: [https://mone27.gitlab.io/ec-data-quality/](https://mone27.gitlab.io/ec-data-quality/) and the paper [https://mone27.gitlab.io/ec-data-quality/term_paper.pdf](https://mone27.gitlab.io/ec-data-quality/term_paper.pdf)

